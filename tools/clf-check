#!/usr/bin/env python

"""Check that channels in the channel list file can be found in GWF files
"""

from __future__ import print_function

import argparse
import gwdatafind
import json
import os
import sys

from collections import OrderedDict

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser

from gwpy.io import gwf as io_gwf
from gwpy.time import (from_gps, to_gps)
from gwpy.timeseries import TimeSeriesDict

__author__ = ('Duncan Macleod <duncan.macleod@ligo.org>, '
              'Reed Essick <reed.essick@ligo.org>, '
              'Patrick Godwin <patrick.godwin@ligo.org> '
              'Alex Urban <alexander.urban@ligo.org>')


# -- utilities

def abspath2(path):
    return os.path.abspath(os.path.expanduser(path))


def find_gwf(frametype, gps=None):
    observatory = frametype[0]
    if gps in [None, 'now']:
        return gwdatafind.find_latest(observatory, frametype,
                                      on_missing='error')[0]
    else:
        gps = to_gps(gps)
        gwf = gwdatafind.find_urls(observatory, frametype, gps, gps,
                                   on_gaps='error')[0]
    return gwf.split('localhost')[-1]


def iter_channel_rates(framefile):
    from LDAStools import frameCPP
    if not isinstance(framefile, frameCPP.IFrameFStream):
        framefile = io_gwf.open_gwf(framefile, 'r')
    toc = framefile.GetTOC()
    for channels, read_data in [
            (toc.GetProc(), framefile.ReadFrProcData),
            (toc.GetADC(), framefile.ReadFrAdcData),
            (toc.GetSim(), framefile.ReadFrSimData),
        ]:
        for channel in channels:
            data = read_data(0, channel)  # one frame per stream
            try:  # check ADC channels
                valid = data.dataValid
            except AttributeError:
                valid = 0
            yield channel, (1./data.data[0].GetDim(0).dx, valid)

# -- parse command line

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

parser.add_argument('channel-list-file', type=abspath2,
                    help='path to channel list file')
parser.add_argument('-t', '--gps', default='now',
                    help='GPS time to make comparison')
parser.add_argument('--json', help='filepath for nagios JSON report')

args = parser.parse_args()

# -- read channel list file and parse channels

cfile = getattr(args, 'channel-list-file')
config = ConfigParser()
read_ = config.read([cfile])
if not read_:
    raise IOError('Failed to read {}'.format(cfile))
print('Read list file [{}]'.format(os.path.basename(cfile)))

allchannels = set()
gwfchannels = dict()
missing = dict()
bad_rates = dict()
invalid = dict()
repeated = list()

for sec in config.sections():
    print('Checking \'{}\':'.format(sec))
    frametype = config.get(sec, 'frametype')
    channels = [c.lstrip().split(None, 1)[0] for
                c in config.get(sec, 'channels').strip().splitlines()]
    for channel in channels:
        if channel in allchannels:
            repeated.append(channel)
            print('        WARNING: repeated channel : '+channel)
        else:
            allchannels.add(channel)
    channels = set(channels)

    rows = [c.lstrip().split(None) for
                c in config.get(sec, 'channels').strip().splitlines()]
    chanrates = dict()
    for row in rows:
        if len(row)!=4:
            print('        WARNING: bad format for : %s'%(' '.join(row)))
        chanrates[row[0]] = int(row[1])

    print('    Identified {} channels'.format(len(channels)))
    if frametype not in gwfchannels:
        print('    Finding channels for {}:'.format(frametype))
        gwf = find_gwf(frametype, gps=args.gps)
        print(repr(gwf))
        gwfb = os.path.basename(gwf)
        print('        Identified GWF: {}'.format(gwfb))
        gwfchannels[frametype] = (gwfb, dict(iter_channel_rates(gwf)))
        print('        Read {} channels and rates'.format(
            len(gwfchannels[frametype][1])))

    ### check for missing channels

    missing[sec] = channels - set(gwfchannels[frametype][1].keys())
    if missing[sec]:
        print('    Found {} missing channels:'.format(len(missing[sec])))
        for name in missing[sec]:
            print('        {}'.format(name))
    else:
        print('    No missing channels')

    ### verify rates
    print('    Verifying rates are consistent and data is valid')
    bad_rates[sec] = []
    invalid[sec] = []
    rates = gwfchannels[frametype][1]
    for chan, rate in chanrates.items():
        if (chan in rates):
            r, v = rates[chan]
            if rate != r:
                bad_rates[sec].append(chan)
            if v:
                invalid[sec].append(chan)

    if bad_rates[sec]:
        print('    Found {} channels with incorrect rates:'.format(
            len(bad_rates[sec])))
        for name in bad_rates[sec]:
            print('\t'.join([
                '%s\n' % name,
                'INI rate = %s\n' % str(chanrates[name]),
                'GWF rate = %s' % str(rates[name])
            ]))
    else:
        print('    No channels with incorrect rates')

    if invalid[sec]:
        print('    Found {} channels with invalid data:'.format(
            len(invalid[sec])))
        for name in invalid[sec]:
            print('        {}'.format(name))
    else:
        print('    No channels with invalid data')

# print missing channels
allmissing = set(name for sec in missing for name in missing[sec])
allbad_rates = set(name for sec in bad_rates for name in bad_rates[sec])
allinvalid = set(name for sec in invalid for name in invalid[sec])
if allmissing or allbad_rates or repeated or allinvalid:
    exitcode = 2
    report = ('{} channels are missing, {} channels are repeated, '
              '{} channels have bad rates, and {} channels are invalid '
              'in {}').format(
                  len(allmissing), len(repeated), len(allbad_rates),
                  len(allinvalid), os.path.basename(cfile))
    print('', file=sys.stderr)
    print(report, file=sys.stderr)
else:
    report = ('There are no missing channels, no repeated channels, '
              'all channels have the correct sampling rate, and no '
              'channles are invalid in {}').format(os.path.basename(cfile))
    print(report)
    exitcode = 0

# write JSON report
if args.json:
    now = to_gps('now').gpsSeconds
    date = str(from_gps(now))
    jdict = OrderedDict([
        ('created_command', ' '.join(sys.argv)),
        ('created_datetime', date),
        ('created_gps', now),
        ('author', {'name': __author__.split('<')[0].strip(),
                    'email': __author__.split('<', 1)[1].split('>')[0]}),
        ('ok_txt', 'All channels in configuration are available in GWF files'),
        ('unknown_txt', 'This monitor is not updating'),
        ('critical_txt', 'One or more channels in configuration are not '
                        'available in GWF files'),
        ('status_intervals', [
            {'num_status': exitcode,
             'start_sec': 0, 'end_sec': 7200,
             'txt_status': report,
            },
            {'num_status': 3, 'start_sec': 7200,
             'txt_status': 'This monitor is not updating',
            },
        ]),
        ('frametypes', []),
        ('report', []),
    ])
    for ft in gwfchannels:
        jdict['frametypes'].append({'name': ft, 'gwf': gwfchannels[ft][0]})
    for key in set(missing.keys() | bad_rates.keys()):
        jdict['report'].append({
            'name': key,
            'missing': list(missing.get(key, [])),
            'bad_rates': list(bad_rates.get(key, [])),
            'invalid': list(invalid.get(key, [])),
        })
    with open(args.json, 'w') as f:
        json.dump(jdict, f, indent=4)

sys.exit(1 if exitcode else 0)
