#
# This script updates channel lists from the old format:
#   channel_name rate
# to the new format:
#   channel_name rate safety fidelity
#

import sys, os, re

inputfile = sys.argv[1]

channels = open(inputfile)
output = open(os.path.splitext(inputfile)[0] + '-syntax.ini', 'w')

for line in channels:

    matchResult = re.search(r'^\s*([HL]1:[-A-Z0-9_]*) [0-9]{1,5}$', line)

    if matchResult:
        line = re.sub(r'([0-9]{1,5})$', r'\1 safe clean', line)

    output.write(line)


channels.close()
output.close()
